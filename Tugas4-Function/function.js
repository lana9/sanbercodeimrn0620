// No. 1
console.log('=== No. 1 ===');
function teriak() {
    var trk = 'Halo Sanbers!';
    return trk;
}
console.log(teriak());

console.log();

// No. 2
console.log('=== No. 2 ===');
function kalikan(x1, x2) {
    return x1*x2;
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

console.log();

// No. 3
console.log('=== No. 3 ===');
function introduce(x1, x2, x3, x4) {
    var hasil = 'Nama saya '
    return hasil.concat(x1).concat(', umur saya ').concat(x2).concat(' tahun, alamat saya di ')
    .concat(x3).concat(', dan saya punya hobby yaitu ').concat(x4).concat('!');
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);