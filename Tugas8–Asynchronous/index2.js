var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

function eks(xt, bks, zbks){
    let val = readBooksPromise(xt, bks);
    val.then(function (fulfilled) {
        if(fulfilled!=xt && books[zbks]!=undefined){
            zbks++;
            if(zbks < books.length){
                eks(fulfilled, books[zbks], zbks);
            }
        }
    })
    .catch(function (error) {
        // console.log("=>"+error.message);
    });
}

eks(4000, books[0], 0);
