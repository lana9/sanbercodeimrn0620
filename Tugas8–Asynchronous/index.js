// Soal No. 1 (Callback Baca Buku)

let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini

function jalankan(xt, bks, zbks){
    readBooks(xt, bks, function(check) {
        if(check!=xt && books[zbks]!=undefined){
            zbks++;
            if(zbks < books.length){
                jalankan(check, books[zbks], zbks);
            }
            
        }
    });
}
jalankan(5000, books[0], 0);