// 1. SOAL CLASS
console.log("1. SOAL CLASS");

class Score {
    constructor(points) {
        points;
        this.subject = "pengetahuan alam";
        this.email = "test@gmail.com";

        console.log("email => "+this.email);
        console.log("subject => "+this.subject);

        let p = Array.from(points);
        
        if(p.length>0){
            this.average(p);
        }else{
            console.log("Nilai => "+points);
        }
    }

    average(z){
        let c = 0;
        for (let q = 0; q < z.length; q++) {
            c = c+z[q];
        }
        let rata = c/z.length;
        console.log("Nilai Rata2 => "+rata);
    }
    
}

var input = [13, 13, 12, 11];
var tscore = new Score(input);
tscore;

console.log();

console.log("2. SOAL Create");

const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

viewScores = (data, subject) => {
    let val = [];
    let obj = {};
    let rat = 0;
    for (let q = 1; q < data.length; q++) {
        let dt = data[q];
        for (let z = 0; z < dt.length; z++) {
            
            if(z==0){
                obj.email = dt[z];
                obj.subject = subject;
            }else{
                rat = rat+dt[z];
            }
        }
        obj.point = rat;
        val.push(obj);
        
    }
    console.log("val => "+val);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

console.log();

console.log("3. SOAL Recap Score");

recapScores = (data) => {
    for (let z = 1; z < data.length; z++) {
        let dt = data[z]
        let n = 0;
        let mail;
        for (let q = 0; q < dt.length; q++) {
            if(q>0){
                n = n+dt[q];
            }else{
                mail = dt[q];
            }
            
        }
        console.log(z)
        console.log("email : "+mail);
        let rat = n/3;
        if(rat > 70){
            console.log("Rata-rata : "+rat);
            console.log("Predikat: participant");
        }else if(rat > 80){
            console.log("Rata-rata : "+rat);
            console.log("Predikat: graduate");
        }else if(rat > 90){
            console.log("Rata-rata : "+rat);
            console.log("Predikat: honour");
        }
        
    }
}

recapScores(data);