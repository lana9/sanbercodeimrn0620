function arrayToObject(x){
    let nama = "";
    let obj = {};
    for (let q = 0; q < x.length; q++) {
        let ar = x[q];
        if((ar[3] == undefined)){
            obj.age = "Invalid Birth Year";
        }
        nama = nama+(q+1)+". ";
        for (let z = 0; z < ar.length; z++) {
            if (z == 0) {
                obj.firstName = ar[z];
                nama = nama+ar[z]+" ";
            } else if (z == 1){
                obj.lastName = ar[z];
                nama = nama+ar[z]+" :";
            } else if (z == 2){
                obj.gender = ar[z];
            } else if (z == 3){
                let dt = new Date();    
                let th = dt.getFullYear()-ar[z];
                if(th>0){
                    obj.age = th;
                }else{
                    obj.age = "Invalid Birth Year";
                }
            }
        }
        console.log(nama,obj);
        nama = "";
    }
}
console.log("=====Soal No. 1 (Array to Object)=====");
let people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);
console.log();
let people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);

console.log();

function shoppingTime(memberId, money) {
    if ((memberId=='') || (memberId == undefined)) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
        let obj = {};
    obj.memberId = memberId;
    obj.money = money;
    
    let ar = [];
    let uang = money;
    if (uang >= 1500000) {
        uang = uang-1500000;
        ar.push("Sepatu Stacattu");
    }
    
    if (uang >= 500000) {
        uang = uang-500000;
        ar.push("Baju Zoro");
    }

    if (uang >= 250000) {
        uang = uang-250000;
        ar.push("Baju H&N");
    }

    if (uang >= 175000) {
        uang = uang-175000;
        ar.push("Sweater Uniklooh");
    }

    if (uang >= 50000) {
        uang = uang-50000;
        ar.push("Casing Handphone");
    }

    if (uang == money) {
        return "Mohon maaf, uang tidak cukup";
    }

    obj.listPurchased = ar;
    obj.changeMoney = uang;
    return obj;
    }
    
}

console.log("=====Soal No. 2 (Shopping Time)=====");
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log();

console.log("=====Soal No. 3 (Naik Angkot)=====");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let po1;
    let po2;
    let val = [];
    for (let q = 0; q < arrPenumpang.length; q++) {
        let obj = {};
        let as = arrPenumpang[q];
        for (let z = 0; z < as.length; z++) {
            switch (z) {
                case 0:
                    obj.penumpang = as[z];
                    break;
                case 1:
                    obj.naikDari = as[z];
                    po1 = ambilnilai(rute, as[z]);
                    break;
                case 2:
                    obj.tujuan = as[z];
                    po2 = ambilnilai(rute, as[z]);
                    break;
                default:
                    break;
            }
        }
        let byr = (po2-po1)*2000;
        obj.bayar = byr;

        val.push(obj);
    }
    return val;
}

function ambilnilai(p1, p2){
    let val;
    for (let index = 0; index < p1.length; index++) {
        if (p1[index] == p2) {
            val = index;
        }
    }
    return val;
}

// naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]);
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));