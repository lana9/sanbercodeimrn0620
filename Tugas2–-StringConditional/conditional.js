// conditional If-else
var nama = "Junaedi";
var peran = "Werewolf";

if(!(nama == "" && peran == "")){
    if(nama == "John" && peran == ""){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
    }else if(nama == "Jane" && peran=="Penyihir"){
        console.log("Selamat datang di Dunia Werewolf, "+nama);
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
    }else if(nama == "Jenita" && peran=="Guard"){
        console.log("Selamat datang di Dunia Werewolf, "+nama);
        console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }else if(nama == "Junaedi" && peran=="Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, "+nama);
        console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!");
    }else if((nama=="") && (peran!="")){
        console.log("Nama harus diisi!");
    }else if((nama!="") && (peran=="")){
        console.log("Pilih Peranmu untuk memulai game");
    }else{
        console.log("Nama & Peran tidak terdeteksi");
    }
}else{
    console.log("Nama harus diisi!");
}

console.log("<==================================>");

// conditional Switch Case
var tanggal = 21; 
var bulan = 15; 
var tahun = 1994;

switch (bulan) {
    case 1:
        console.log(tanggal+" Januari "+tahun);
        break;
    case 2:
        console.log(tanggal+" Februari "+tahun);
        break;
    case 3:
        console.log(tanggal+" Maret "+tahun);
        break;
    case 4:
        console.log(tanggal+" April "+tahun);
        break;
    case 5:
        console.log(tanggal+" Mei "+tahun);
        break;
    case 6:
        console.log(tanggal+" Juni "+tahun);
        break;
    case 7:
        console.log(tanggal+" Juli "+tahun);
        break;
    case 8:
        console.log(tanggal+" Agustus "+tahun);
        break;
    case 9:
        console.log(tanggal+" September "+tahun);
        break;
    case 10:
        console.log(tanggal+" Oktober "+tahun);
        break;
    case 11:
        console.log(tanggal+" November "+tahun);
        break;
    case 12:
        console.log(tanggal+" Desember "+tahun);
        break;
    default:
        console.log("Input bulan salah")
        break;
}