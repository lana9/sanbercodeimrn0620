// No. 1 Looping While
console.log("===(No. 1 Looping While)===");
var trik = 1;
var val2 = 20;
var a = 1;
while (a<=20) {
    if (a%2==0) {
        if(trik==1){
            console.log(a+" - I love coding");
                if(a==20){
                    trik = 2;
                    a = 1;
                    continue;
                }
        }else if (trik==2) {
            console.log(val2+" - I will become a mobile developer");
            val2 = val2-2;
        }    
    }else{
        if (a==1 && trik==1) {
            console.log("*LOOPING PERTAMA*");
        } else if (a==1 && trik==2) {
            console.log("*LOOPING KEDUA*");
        }
    }
    a++;
}

console.log("");

// No. 2 Looping menggunakan for
console.log("===(No. 2 Looping menggunakan for)===");
for (var a = 1; a <= 20; a++) {
    
    if (a%2==0) {
        console.log(a+" - Berkualitas");
    } else {
        if (a%3==0) {
            console.log(a+" - I Love Coding");
        } else {
            console.log(a+" - Santai");
        }
    }
    
}

console.log("");

// No. 3 Membuat Persegi Panjang
console.log("===(No. 3 Membuat Persegi Panjang)===");
var a = 1;
while (a<=4) {
    var hasil = "";
    for (var z = 0; z < 8 ; z++) {
        hasil = hasil+"#";
    }
    console.log(hasil);
    a++;
}

console.log("");

// No. 4 Membuat Tangga
console.log("===(No. 4 Membuat Tangga)===");
var q = "";
for(var a=0; a<7; a++){
    for(var b=0; b<=0; b++){
        q=q+"#";
    }
    console.log(q);
}

console.log("");

// No. 5 Membuat Papan Catur
console.log("===(No. 5 Membuat Papan Catur)===");

var x1 = " # # # #";
var x2 = "# # # # ";
for(var a=0; a<8; a++){
    if(a%2==0){
        console.log(x1);
    }else{
        console.log(x2);
    }   
}
