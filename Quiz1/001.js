function terbalik(x) {
    let nilai = "";
    for (let q = (x.length-1); q >= 0; q--) {
        nilai = nilai+x[q];
    }
    return nilai;
}

function maksimum(num1, num2) {
    let nilai = 0;
    if(num1!=undefined && num2==undefined){
        nilai = num1;
    }else if((num1==num2) || (num1<0) || (num2<0) || ((num1==undefined)&&(num2==undefined))){
        nilai = -1;
    }else{
        if(num1>num2){
            nilai = num1;
        }else{
            nilai = num2;
        }
    }
    
    return nilai;
}

function palindrome(x) {
    let tampung = terbalik(x);
    if (tampung==x) {
        return true;
    }else{
        return false;
    }
}


// TEST CASES String Terbalik
console.log("<===TEST CASES String Terbalik===>");
console.log(terbalik("abcde")) // edcba
console.log(terbalik("rusak")) // kasur
console.log(terbalik("racecar")) // racecar
console.log(terbalik("haji")) // ijah
console.log();

// TEST CASES Bandingkan Angka
console.log("<===TEST CASES Bandingkan Angka===>");
console.log(maksimum(10, 15)); // 15
console.log(maksimum(12, 12)); // -1
console.log(maksimum(-1, 10)); // -1
console.log(maksimum(112, 121));// 121
console.log(maksimum(1)); // 1
console.log(maksimum()); // -1
console.log(maksimum("15", "18")) // 18
console.log();

// TEST CASES Palindrome
console.log("<===TEST CASES Palindrome===>");
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false