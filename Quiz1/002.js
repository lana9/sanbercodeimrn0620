function DescendingTen(num) {
    let hasil = "";
    if(num==undefined){
        return "-1";
    }else{
        let cek = 0
        for (let q = num; q >0; q--) {
            cek++;
            if(cek<=10){
                hasil = hasil+" "+q;
            }else{
                q=0;
            }
        }
        return hasil;
    }
}

function AscendingTen(num) {
    let hasil2 = "";
    if(num==undefined){
        return "-1";
    }else{
        let cek = 0
        for (let q = num; q <1000; q++) {
            cek++;
            if(cek<=10){
                hasil2 = hasil2+" "+q;
            }else{
                q=1000;
            }
        }
        return hasil2;
    }
}

function ConditionalAscDesc(reference, check) {
    let has = "";

    if((reference==undefined&&check==undefined) || (check==undefined)){
        has = "-1";
    }else{
        if(check%2==0){
            has = DescendingTen(reference);
        }else{
            has = AscendingTen(reference);
        }
    }
    
    return has;
}

console.log("<===TEST CASES Descending Ten===>");
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
console.log();

// TEST CASES Ascending Ten
console.log("<===TEST CASES Ascending Ten===>");
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1
console.log();

// TEST CASES Conditional Ascending Descending
console.log("<===TEST CASES Conditional Ascending Descending===>");
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
console.log();