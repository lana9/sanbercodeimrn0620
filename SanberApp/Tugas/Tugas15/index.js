import React from "react";
import {NavigationContainer } from '@react-navigation/native';
import {createStackNavigator } from '@react-navigation/stack';

import { SignIn, CreateAccount, Profile, Home } from '.Screens';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name = "Home" component= {Home}/>
  </HomeStack.Navigator>
)

const SearchStackScreen = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name = "Home" component= {Home}/>
  </SearchStack.Navigator>
)

export default () => (
  <NavigationContainer>
    <Tabs.Navigator>
      <Tabs.Screen name = "Home" component= {HomeStackScreen}/>

      <Tabs.Screen name = "Profile" component= {SearchStackScreen}/>  
    </Tabs.Navigator>
  </NavigationContainer>

  // <NavigationContainer>
  //   <AuthStack.Navigator>
  //     <AuthStack.Screen name="SignIn" component={SignIn} options={{title: 'Sign In'}} />
  //     <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title: 'Create Account'}} />
  //    </AuthStack.Navigator>
  // </NavigationContainer>
);