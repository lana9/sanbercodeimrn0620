import React from 'react';
import { View,StyleSheet,Image, TouchableOpacity,Text } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


export default class SkilItem extends React.Component {
    render(){
        let skill=this.props.skill;
        return (
            <View style={styles.container}>
                <View style={{borderWidth:0,backgroundColor:'#B4E9FF',borderRadius:15,margin:3,flexDirection:'row',}}>
                    <TouchableOpacity style={{margin:19}} >
                        <MaterialCommunityIcons name={skill.iconName} size={80} color="black" />
                    </TouchableOpacity>
                    <View style={{flexDirection:'column',width:150}}>
                        <View style={{borderWidth:0}}>
                            <Text style={{color:'#003366',fontSize:20,margin:5,fontWeight:'bold'}}>{skill.skillName}</Text>
                        </View>
                        <View style={{borderWidth:0}}>
                        <Text style={{color:'#3EC6FF',fontSize:10,margin:5,fontWeight:'bold'}}>{skill.categoryName}</Text>
                        </View>
                        <View style={{borderWidth:0,justifyContent:'flex-end',alignItems:'flex-end',}}>
                            <Text style={{color:'#FFFFFF',fontSize:40,margin:5,fontWeight:'bold'}}>{skill.percentageProgress}</Text>
                        </View>
                    </View>
                    <View style={{margin:19,justifyContent:'center',alignItems:'center',left:1 }} >
                         <MaterialCommunityIcons name="chevron-right" size={70} color="black" />
                    </View>
                   
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
    },

});
