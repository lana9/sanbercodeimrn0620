import React from 'react';
import { View, Text,Image, ScrollView, TextInput, TouchableOpacity, StyleSheet,FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import data from './skillData.json'
import Skillitem from './components/SkilItem'

export default class SkillScreen extends React.Component {
    render() {
      return (
        <View style={styles.container}>
             <View style={{margin:10,borderColor:'yellow',borderWidth:0}}></View>
            <View style={styles.header}>
                 <Image source={require('../Tugas13/images/logo.png')} style={{width:200,height:50}} />
            </View>
             <View style={styles.headernama}>
                <Icon style={{width:30,color:'#3EC6FF',left:10,marginTop:5}} name="account-circle" size={30}></Icon>
                <Text style={{color:'#3EC6FF',left:50,marginTop:-35}}>Hai</Text>
                <Text style={{color:'#3EC6FF',left:50,marginTop:-2}}>Irvan Maulana</Text>
            </View>
            <View style={{borderWidth:0}}>
            <Text style={{color:'#003366',left:10,fontSize:35}}>SKILL</Text>
            </View>
            <View style={{borderWidth:0,flexDirection:'row',alignItems: 'center',justifyContent: 'center'}}>
                <View style={{borderWidth:0,backgroundColor:'#B4E9FF',alignItems:'center',borderRadius:9,margin:1}}>
                     <Text style={{color:'#003366',fontSize:13,margin:5,fontWeight:'bold'}}>Library / Framework</Text>
                </View>
                <View style={{borderWidth:0,backgroundColor:'#B4E9FF',alignItems:'center',borderRadius:9,margin:1}}>
                     <Text style={{color:'#003366',fontSize:13,margin:5,fontWeight:'bold'}}>Bahasa Pemrograman</Text>
                </View>
                <View style={{borderWidth:0,backgroundColor:'#B4E9FF',alignItems:'center',borderRadius:9,margin:1}}>
                     <Text style={{color:'#003366',fontSize:13,margin:5,fontWeight:'bold'}}>Teknologi</Text>
                </View>
            </View>
            <View style={{borderWidth:0}}>
            <FlatList 
                data={data.items}
                renderItem={(skill)=> <Skillitem skill={skill.item} />}
                keyExtractor={(skill)=> (skill.id+'')}
                ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                />
            </View>
                  
        </View>
      )
    }
  }


const styles=StyleSheet.create({
container:{
    flex:1,
    borderColor:'blue',
    borderWidth:0
   
},
header:{
    //top:18,
   // paddingTop:1,
    alignItems: 'flex-end',
    borderColor:'blue',
    borderWidth:0,
   
   // borderWidth:0,
    //backgroundColor: '#E91E63',
     
},
headernama:{
    
   // backgroundColor: '#FDD7E4',
   borderWidth:0,
   borderColor:'red',
   // alignSelf: 'stretch',
    // textAlign: 'center',
    // margin:10

    
},
});
