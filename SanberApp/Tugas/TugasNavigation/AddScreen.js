import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class AddScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textltittle}>Halaman Proyek</Text>
            </View>
            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    textltittle: {
        fontSize: 24,
        color: '#2f354b',
        textAlign: 'center',
        justifyContent: 'center',
        paddingTop: 250
    },
});