import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class AboutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textltittle}>Tentang Saya</Text>
                <Icon style={styles.user} name="user-circle" size={150} />
                <Text style={styles.textname}>Irvan Maulana</Text>
                <Text style={styles.textabout}>React Native Developer</Text>
                <View style={styles.body}>
                    <View style={styles.bodytext} >
                        <Text style={styles.textportofolio}>Portofolio</Text>
                        <Text style={styles.textline}>__________________________________________________________________</Text>
                        <View style={styles.bodygit} >
                            <Icon style={styles.user} name="gitlab" size={60} color="#3EC6FF"/>
                            <Icon style={styles.user} name="github" size={60} color="#3EC6FF"/>
                        </View>
                        <View style={styles.bodygit} >
                            <Text style={styles.textgit}>@lana9</Text>
                            <Text style={styles.textgit}>@lana_9</Text>
                        </View>
                        
                    </View>
                </View>
                <View style={styles.body2}>
                    <View style={styles.bodytext2} >
                        <Text style={styles.textportofolio}>Hubungi Saya</Text>
                        <Text style={styles.textline}>__________________________________________________________________</Text>
                        <View>
                            <View style={styles.bodykontak} >
                                <Icon style={styles.user} name="facebook" size={60} color="#3EC6FF"/>
                                <Icon style={styles.user} name="instagram" size={60} color="#3EC6FF"/>
                                <Icon style={styles.user} name="twitter" size={60} color="#3EC6FF"/>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textltittle: {
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: 36,
        color: '#2f354b',
        textAlign: 'center',
        justifyContent: 'center',
        paddingTop: 70
    },
    user: {
        paddingTop: 10,
        textAlign: 'center',
        justifyContent: 'center',
    },
    textname: {
        fontSize: 25,
        fontWeight: "bold",
        paddingTop: 10,
        textAlign: 'center',
        justifyContent: 'center',
    },
    textabout: {
        fontSize: 15,
        paddingTop: 10,
        textAlign: 'center',
        justifyContent: 'center',
        color: '#3EC6FF'
    },
    body: {
        padding:10,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    bodytext: {
        width: 380,
        height: 150,
        backgroundColor: '#EFEFEF',
        borderRadius: 20,
        borderStartColor: "red"
    },
    textportofolio: {
        padding:5,
        fontSize: 15,
        textAlign: 'left',
        justifyContent: 'center',
        color: '#003366'
    },
    textline: {
        top: -15,
        fontSize: 14,
        textAlign: 'left',
        justifyContent: 'center',
        color: '#003366'
    },
    bodygit: {
        flexDirection: "row",
        justifyContent: "space-around"
    },
    textgit: {
        padding:5,
        fontSize: 15,
        textAlign: 'left',
        justifyContent: 'center',
        color: '#003366'
    },
    bodykontak: {
        left: 150,
        top : -25,
        alignItems: "flex-start",
        flexDirection: "column",
        justifyContent: "flex-end"
    },
    body2: {
        top:-65,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    bodytext2: {
        width: 380,
        height: 250,
        backgroundColor: '#EFEFEF',
        borderRadius: 20,
        borderStartColor: "red"
    }
});