import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';

export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style={styles.imagelogo} />
                <Text style={styles.textltittle}>Login</Text>
                <TextInput style={styles.textinput} placeholder='Username / Email' />
                <TextInput style={styles.textinput} placeholder='Password' />
                <TouchableOpacity style={styles.appButtonContainer}>
                    <Text style={styles.appButtonText}>Masuk</Text>
                </TouchableOpacity>
                <Text style={styles.textltittle2}>atau</Text>
                <TouchableOpacity style={styles.appButtonContainer2}>
                    <Text style={styles.appButtonText2}>Daftar ?</Text>
                </TouchableOpacity>
            </View>
            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    imagelogo : {
        width: 390,
        height: 100,
        padding: 60,
        margin:20,
        position: "absolute",
        top:50
    },
    textltittle: {
        fontSize: 24,
        color: '#2f354b',
        textAlign: 'center',
        justifyContent: 'center',
        paddingTop: 250
    },
    textinput: {
        margin: 30,
        height: 40,
        width: 350,
        borderColor: 'black',
        borderWidth: 1,
        paddingHorizontal: 10,
    },
    textltittle2: {
        top:60,
        fontSize: 23,
        color: '#3EC6FF',
        textAlign: 'center',
        justifyContent: 'center',
    },
    appButtonContainer: {
        backgroundColor: "#3EC6FF",
        borderRadius: 16,
        paddingVertical: 10,
        paddingHorizontal: 12,
        position: "absolute",
        width: 140,
        left: 135,
        top: 493
    },
    appButtonText: {
        fontSize: 15,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center"
    },
    appButtonContainer2: {
        backgroundColor: "#003366",
        borderRadius: 16,
        paddingVertical: 10,
        paddingHorizontal: 12,
        position: "absolute",
        width: 140,
        left: 135,
        top: 580
    },
    appButtonText2: {
        fontSize: 15,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center"
    }
});