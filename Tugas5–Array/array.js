// Soal No. 1 (Range)
console.log("#Soal No. 1 (Range)");
function range(startNum, finishNum) {
    var value = [];
    if (startNum==undefined || finishNum==undefined) {
        value = -1;
    } else if(startNum<finishNum){
        while (startNum<=finishNum) {
            value.push(startNum);
            startNum++;
        }
    }else{
        while (startNum>=finishNum) {
            value.push(startNum);
            startNum--;
        }
    }
    return value;
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

console.log();

// Soal No. 2 (Range with Step)
console.log("#Soal No. 2 (Range with Step)");
function rangeWithStep(x1, x2, x3) {
    var value = [];
    var q = 1;
    var z = 0;
    if (x1<x2) {
        while (x1<=x2) {
            if (x3==1) {
                value.push(x1);
            } else {
                if ((q==1) || (z == 0)) {
                    value.push(x1);
                    q = 0;
                    z = (x3-1);
                }else{
                    z--;
                } 
            }
            x1++;
        }
    } else {
        while (x1>=x2) {
            if (x3==1) {
                value.push(x1);
            } else {
                if ((q==1) || (z == 0)) {
                    value.push(x1);
                    q = 0;
                    z = (x3-1);
                }else{
                    z--;
                } 
            }
            x1--;
        }
    }
    
    return value;
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log();

// Soal No. 3 (Sum of Range)
console.log("#Soal No. 3 (Sum of Range)");
function sum(x1, x2, x3) {
    var tampung;
    var nilai = 0;
    if (x1 == undefined && x2 == undefined && x3 == undefined) {
        nilai = 0;
    } else if (x2 == undefined && x3 == undefined){
        nilai = 1;
    } else if (x3 == undefined){
        tampung = range(x1, x2);
        for (var c = 0; c < tampung.length; c++) {
            nilai = nilai+tampung[c];
        }
    }else{
        tampung = rangeWithStep(x1, x2, x3);
        for (var c = 0; c < tampung.length; c++) {
            nilai = nilai+tampung[c];
        }
    }
    return nilai;
}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log();

// Soal No. 4 (Array Multidimensi)
console.log("#Soal No. 4 (Array Multidimensi)");
function dataHandling(x1) {
    for (var z = 0; z < x1.length; z++) {
        var n = x1[z];
        for (var q = 0; q < n.length; q++) {
            console.log("Nomor ID: "+n[0]);
            console.log("Nama Lengkap: "+n[1]);
            console.log("TTL: "+n[2]+" "+n[3]);
            console.log("Hobi: "+n[4]);
            console.log("===========================");
        }
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);

console.log();

// Soal No. 5 (Balik Kata)
console.log("#Soal No. 5 (Balik Kata)");
function balikKata(x1) {
    var nilai = "";
    for (var q = (x1.length-1); q >= 0; q--) {
        nilai = nilai+x1[q];
    }
    return nilai;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log();

// Soal No. 6 (Metode Array)
console.log("#Soal No. 6 (Metode Array)");
function dataHandling2(dataarr) {
    dataarr.splice(1, 1, dataarr[1]+" Elsharawy");
    dataarr.splice(2, 1, "Provinsi "+dataarr[2]);
    dataarr.splice(4, 2, "Pria","SMA Internasional Metro");
    console.log(dataarr);
    var tgl =(dataarr[3]).split('/')
    switch(tgl[1]) {
        case '01':   {
             console.log(' Januari '); 
             break; 
            }
        case '02':   {
            console.log(' Febuari '); 
            break; 
            }
        case '03':   {
            console.log(' Maret '); 
            break; 
            }
        case '04':   {
            console.log(' April '); 
            break; 
            }
        case '05':   {
            console.log(' Mei '); 
            break; 
            }
        case '06':   {
            console.log(' Juni '); 
            break; 
            }
        case '07':   {
            console.log(' Juli '); 
            break; 
            }
        case '08':   {
            console.log(' Agustus '); 
            break; 
            }
        case '09':   {
            console.log(' September '); 
            break; 
            }
        case '10':   {
            console.log(' Oktober '); 
            break; 
            }
        case '11':   {
            console.log('  Novenber '); 
            break; 
            }
        case '12':   {
            console.log('  Desember '); 
            break; 
            }
    
        }
        var tgl2 = tgl.join("-")
        tgl.sort(function(a, b){return b-a});
        console.log(tgl); 
        console.log(tgl2); 
        var irisan1 = (dataarr[1]).slice(0,14) 
        console.log(irisan1); 
    return dataarr;
}


var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);