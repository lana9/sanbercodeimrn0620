// 1. Mengubah fungsi menjadi fungsi arrow
console.log("1. Mengubah fungsi menjadi fungsi arrow");

const golden = goldenFunction = () => {
    console.log("this is golden!!")
}
   
golden();

console.log();

// 2. Sederhanakan menjadi Object literal di ES6
console.log("2. Sederhanakan menjadi Object literal di ES6");

const newFunction = literal = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
        return 
      }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName();

console.log();

// 3. Destructuring
console.log("3. Destructuring");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

console.log();

// 4. Array Spreading
console.log("4. Array Spreading");

let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];
west = [west,east];
//Driver Code
console.log(west);

console.log();

// 5. Template Literals
console.log("5. Template Literals");

const planet = "earth";
const view = "glass";

let before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
             
// Driver Code
//
console.log(before);